+++
title = "Code"
date = "2021-04-09"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Hugo Authors"
+++


<u>**About**</u>

Please install the [requirements](https://gitlab.com/tomscreektrail/website/-/tree/master/code_and_data/requirements.txt) before running the code. The jupyter notebook containing the code could be found [here](https://gitlab.com/tomscreektrail/website/-/blob/master/code_and_data/Project_ECE5554.ipynb). The data is uploaded [here](https://gitlab.com/tomscreektrail/website/-/tree/master/code_and_data/Data) and the images are uploaded [here](https://gitlab.com/tomscreektrail/website/-/tree/master/code_and_data/figures).
