+++
title = "Project Report"
date = "2021-05-12"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Hugo Authors"
+++

## <u>**Abstract**</u>

Researchers in the field of air quality monitoring have traditionally relied on Aerosol Optical Depth(AOD) as an important parameter to monitor. However, due to a dearth of modern image processing specialists being involved in the field, the majority of teams end up using poorly processed data on which outdated techniques have been applied. When it comes to AOD in particular, this is seen to manifest as unnatural lines in the image. We suggest using edge detection algorithms to refine the data, and then operate on just those data points to get a smoother, and more accurate image. The goal of this project is to provide any team looking to work on satellite a “cleaner” data which should in turn boost their ability to draw more meaningful conclusions from the data.


## <u>**Introduction**</u>
Satellite data is increasingly being used to predict the quality of air and model changes in air quality over time. Moreover, such datasets can be utilised for several applications like health impact studies, source apportionment, and such. However, if this data (obtained primarily from sun-synchronous satellites) is used over smaller temporal resolutions of between 1 and 30 days, or if the values obtained across two different times are merged, unnatural patterns/edges emerge due to a vast number of reasons ranging from the unavailability of data on a particular scan, the swath length of the instrument, or the obscuring of surfaces due to clouds or other environmental phenomena.

The dataset we use is available as .hdf(hierarchical data format) files, and once they are plotted using standard plotting software, aberrations can be noticed at points where any of the aforementioned factors have distorted the data values. Most teams involved in remote sensing research tend to use kernel smoothing to make the dataset smooth over the edges like in [*Dey et al.*](https://www.mdpi.com/2072-4292/12/23/3872), [*Kloog et al.*](https://www.sciencedirect.com/science/article/abs/pii/S1352231011009125?casa_token=ZeSj5M1RF7oAAAAA:w-_7rv1sApc7__UnQptZdrhLdjGUu4LsyWPMnRxJMNv1nbuN6t__uxbUnKbYXwKzQXZ03uQtdfw), etc. This spatial smoothing creates more uncertainty since using the data from adjacent pixels to modify the intensity of other pixels greatly impacts the quality of data that can be retrieved.

Despite there being several image transformation and edge detection algorithms in computer vision libraries like OpenCV, scikit-image, etc, they are designed to handle RGB or grayscale images only. This creates an issue when one has to employ them on satellite data, which mainly sees intensity values fluctuate between 0 and 1, and attempts to scale the satellite data to grayscale , leads to the trading off of data variability during up-scaling and down-scaling.


## <u>**Approach**</u>

Our approach to this problem involved writing a python script to read the input AOD data  from NASA’s MAIAC dataset namely the [*MCD19A2*](https://modis-land.gsfc.nasa.gov/MAIAC.html) .We took one tile of data over North America of AOD at 550 nm for one day to test the working of our algorithm. This particular dataset combines the data collected by the MODIS (Moderate Resolution Imaging Spectroradiometer) instrument hosted aboard NASA’s Aqua and Terra satellites.

Our approach involves first using the delta_change(datain,cutoff) function to detect the edges in the data provided. We took insperation of our algorithm from canny edge detector algorithm. 

{{< image src="/img/canny_edge.jpg" alt="Hello Friend" position="center" style="border-radius: 2px;" >}}
Figure 1: An illustration for edge detection using derivative method for a 1 dimentional change

We detected the edges by comparing the rate-of-change of pixel intensities amongst neighboring pixels in all directions. The “cutoff” parameter is used in order to consider only the pixels whose intensities lie in the upper “cutoff” percentile. 

{{< image src="/img/detection_illustration.png" alt="Hello Friend" position="center" style="border-radius: 2px;" >}}
Figure 2: An illustration for edge detection delta_change() kernel

We estimated the 70th percentile as an optimum cutoff value through trial and error. This particular exercise is carried out to avoid unnecessary edges being ”detected” for pixels who’s intensities are low enough for even a small change to reflect as a drastically high rate-of-change. Thereon, the remaining pixels are all compared against their surrounding pixel intensities (in all 8 directions), and the maximum rate-of-change amongst all these directions are subsequently returned. During execution, this function is called twice to mimic the double differentiation of input values, thereby further refining the points where smoothening techniques are to be employed.

The interpolate_data(original_data,target_data,gama) is aimed at smoothening the values in and around the selected data-points(or edges). The “gama” value signifies the number of pixels that we want to be smoothened around the chosen pixel.The value assigned to the selected pixels are obtained by simply computing the mean values of intensities of a 5x5 window centered at the point under consideration.

{{< image src="/img/gama.jpg" alt="Hello Friend" position="center" style="border-radius: 2px;" >}}
Figure 3: An illustration for the change in window for different gama values

## <u>**Experiments and Results**</u>

We started our experimentation with a single image from MCD19A2 dataset. Please do note that the original dataset is complex and have curviliniar projection (due to shape of earth) which was converted to rectilinear projection prior to it being used in our code.  First we tried single differentiation (one time using the  delta_change()) to get the edges but it was detecting too many points and incleasing the cutoff parameter the edge data was getting cut. So, we used the double differentiation approach (one time using the  delta_change()) and found the 70 percentile cutoff to be optimum in both pass. We then tested this method on 2 different files and the results for all 3 files are given in qualitative results section.

The naive approach smoothes all of the data but, using our approach we altered only ~5% of the total available pixels (Table 1) to smoothened the data. The major edges were detected and smoothened out by using our approach as demostrated in figure 5 and our approach could also be extended for better result if we couple it with clustering algorithm to remove detected noise.

## <u>**Qualitative Results**</u>

{{< image src="/img/pixel_table.jpg" alt="Hello Friend" position="center" style="border-radius: 2px;" >}}

Table 1: The total number of points and the number of points detected in target dataset

{{< image src="/img/figure_comparison.jpg" alt="Hello Friend" position="center" style="border-radius: 2px;" >}}
Figure 4: The tiles for 3 different day and the spatial distribution of target dataset

{{< image src="/img/zoomed_comparison.jpg" alt="Hello Friend" position="center" style="border-radius: 2px;" >}}
Figure 5: The comparison of raw and smoothened dataset for each of the day 

## <u>**Conclusion**</u>

This report has described the problems plaguing research teams who aim to use open source satellite data for their analysis. We have chosen to focus on a specific component which is the AOD, but this work can also be extended to other parameters as well. We identified the need to provide a more elegant solution than what is broadly prevalent in this particular field. By using a simple edge detection algorithm over a generalized kernel based solution, we show a marked improvement in the quality of data that teams can now rely upon, whilst still maintaining data integrity.

Our Team:

* Rajarshi Mukherjee
* Kuldeep Dixit

Learn more about the project by contacting Prof. [*Lynn Abbott*](https://ece.vt.edu/people/profile/abbott).

