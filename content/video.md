+++
title = "Video"
date = "2021-04-09"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Hugo Authors"
+++

<u>**Project Video link**</u>

Please click [here](https://gitlab.com/tomscreektrail/website/-/tree/master/code_and_data/CV_Project_Fall2021.mp4) to view/access the short video of our project.
