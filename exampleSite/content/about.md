+++
title = "About"
date = "2014-04-09"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Hugo Authors"
+++

Trails are **adventurous** for Blacksburg community.

This website is created to take feedback from the Blacksburg residents for trail connectivity project:

Project team members:

* xxxxxxx
* yyyyyyy
* zzzzzzz
* aaaaaaa
* b

Learn more about the project by contacting Prof. [Todd Schenk](https://spia.vt.edu/people/faculty/schenk.html).
